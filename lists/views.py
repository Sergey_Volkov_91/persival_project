from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.


def home_page(request):
    """домашняя страница"""
    if request.method == 'post':
        return HttpResponse(request.POST['item text'])
    return render(request, 'home.html', {'new_item_text': request.POST.get('item_text', '')})


